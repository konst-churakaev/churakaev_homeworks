package ru.pcs.web.services.rezult.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.web.model.GtiOper;
import ru.pcs.web.model.Oper;
import ru.pcs.web.model.RezGti;
import ru.pcs.web.repositories.Operation.OperRepository;
import ru.pcs.web.repositories.rezult.RezGtiRepository;
import ru.pcs.web.services.rezult.RezGtiService;
import ru.pcs.web.start.StartJob;

import java.io.*;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RezGtiServiceImp implements RezGtiService {

    private final RezGtiRepository rezOperRepository;
    private final OperRepository operRepository;

    @Override
    public RezGti setDailyReport(LocalDate dateOper) {

        RezGti rezGti = rezOperRepository.getDailyReport(dateOper);

        BufferedWriter bufferedWriter;

        List<GtiOper> gtiOpers = getAllGti();

        try {
            Writer writer = new FileWriter("Суточный рапорт.txt", Charset.forName("utf-8"));
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write("Общее время-" + getOperation().getTimeAll() + "|  Время циркуляции-" + getOperation().getTimeCycle() +
                    "|  Время бурения-" + getOperation().getTimeByr() + "|  Время СПО-" + +getOperation().getTimeSpo() + "|  Врямя замеров-" + getOperation().getTimeZamer() +
                    "|  Время проработки-" + getOperation().getTimeProrabot() + "|  Время промывки-" + getOperation().getTimeProm() +
                    "|  Время наращивания-" + getOperation().getTimeNarash() + "| Время КНБК-" + getOperation().getTimeKnbk() + "|  Время ремонта-" + getOperation().getTimeRemont() +
                    "|  Время ГИС-" + getOperation().getTimeGis() + "|  Время НПВ" + getOperation().getTimeNpv() + "| Прочее" + getOperation().getTimeProchee());

            bufferedWriter.newLine();
            bufferedWriter.flush();

            for (GtiOper gtiOper : gtiOpers) {
                writer.write(gtiOper.getDateOper() + "|" + gtiOper.getTimeOn() + "|" + gtiOper.getTimeOf() + "|" + gtiOper.getTimeOper() + "|" + gtiOper.getDepth() + "|" + gtiOper.getOperation() + "|" + gtiOper.getNotes());
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rezGti;
    }

    private List<GtiOper> getAllGti() {
        List<Oper> opers = operRepository.findAllGti(LocalDate.now().minusDays(1));
        List<GtiOper> gtiOpers = new ArrayList<>();
        LocalTime timeOn;

        for (int i = 0; i < opers.size(); i++) {
            Oper oper = opers.get(i);

            Integer id = i;
            LocalDate dateOper = oper.getDateOper();
            LocalTime timeOf = oper.getTime();
            if (i == 0) {
                timeOn = StartJob.timeOn;
            } else {
                timeOn = opers.get(i - 1).getTime();
            }

            double timeOper = oper.getTimeOper();
            Double depth = oper.getDepth();
            String operation = oper.getOperation();
            String notes = oper.getNotes();

            GtiOper gtiOper = new GtiOper(id, dateOper, timeOn, timeOf, timeOper, depth, operation, notes);

            gtiOpers.add(id, gtiOper);
        }
        return gtiOpers;
    }

    @Override
    public RezGti getOperation() {

        RezGti rezGti = rezOperRepository.findOperation();

        if (rezGti.getTimeAll() == null) {
            rezGti.setTimeAll(0.0);
        } else {
            rezGti.setTimeAll(getMathRound(rezGti.getTimeAll()));
        }
        if (rezGti.getTimeCycle() == null) {
            rezGti.setTimeCycle(0.0);
        } else {
            rezGti.setTimeCycle(getMathRound(rezGti.getTimeCycle()));
        }
        if (rezGti.getTimeByr() == null) {
            rezGti.setTimeByr(0.0);
        } else {
            rezGti.setTimeByr(getMathRound(rezGti.getTimeByr()));
        }
        if (rezGti.getTimeSpo() == null) {
            rezGti.setTimeSpo(0.0);
        } else {
            rezGti.setTimeSpo(getMathRound(rezGti.getTimeSpo()));
        }
        if (rezGti.getTimeZamer() == null) {
            rezGti.setTimeZamer(0.0);
        } else {
            rezGti.setTimeZamer(getMathRound(rezGti.getTimeZamer()));
        }
        if (rezGti.getTimeProrabot() == null) {
            rezGti.setTimeProrabot(0.0);
        } else {
            rezGti.setTimeProrabot(getMathRound(rezGti.getTimeProrabot()));
        }
        if (rezGti.getTimeProm() == null) {
            rezGti.setTimeProm(0.0);
        } else {
            rezGti.setTimeProm(getMathRound(rezGti.getTimeProm()));
        }
        if (rezGti.getTimeNarash() == null) {
            rezGti.setTimeNarash(0.0);
        } else {
            rezGti.setTimeNarash(getMathRound(rezGti.getTimeNarash()));
        }
        if (rezGti.getTimeKnbk() == null) {
            rezGti.setTimeKnbk(0.0);
        } else {
            rezGti.setTimeKnbk(getMathRound(rezGti.getTimeKnbk()));
        }
        if (rezGti.getTimeRemont() == null) {
            rezGti.setTimeRemont(0.0);
        } else {
            rezGti.setTimeRemont(getMathRound(rezGti.getTimeRemont()));
        }
        if (rezGti.getTimeGis() == null) {
            rezGti.setTimeGis(0.0);
        } else {
            rezGti.setTimeGis(getMathRound(rezGti.getTimeGis()));
        }
        if (rezGti.getTimeNpv() == null) {
            rezGti.setTimeNpv(0.0);
        } else {
            rezGti.setTimeNpv(getMathRound(rezGti.getTimeNpv()));
        }
        if (rezGti.getTimeProchee() == null) {
            rezGti.setTimeProchee(0.0);
        } else {
            rezGti.setTimeProchee(getMathRound(rezGti.getTimeProchee()));
        }
        return rezGti;
    }

    private Double getMathRound(Double timeOper) {
        timeOper = Double.valueOf(Math.round(timeOper * 100));
        return timeOper/100;
    }


    @Override
    public RezGti getDailyReport() {

        RezGti rezGti = rezOperRepository.getDailyReport(LocalDate.now().minusDays(1));

        if (rezGti.getTimeAll() == null) {
            rezGti.setTimeAll(0.0);
        } else {
            rezGti.setTimeAll(getMathRound(rezGti.getTimeAll()));
        }
        if (rezGti.getTimeCycle() == null) {
            rezGti.setTimeCycle(0.0);
        } else {
            rezGti.setTimeCycle(getMathRound(rezGti.getTimeCycle()));
        }
        if (rezGti.getTimeByr() == null) {
            rezGti.setTimeByr(0.0);
        } else {
            rezGti.setTimeByr(getMathRound(rezGti.getTimeByr()));
        }
        if (rezGti.getTimeSpo() == null) {
            rezGti.setTimeSpo(0.0);
        } else {
            rezGti.setTimeSpo(getMathRound(rezGti.getTimeSpo()));
        }
        if (rezGti.getTimeZamer() == null) {
            rezGti.setTimeZamer(0.0);
        } else {
            rezGti.setTimeZamer(getMathRound(rezGti.getTimeZamer()));
        }
        if (rezGti.getTimeProrabot() == null) {
            rezGti.setTimeProrabot(0.0);
        } else {
            rezGti.setTimeProrabot(getMathRound(rezGti.getTimeProrabot()));
        }
        if (rezGti.getTimeProm() == null) {
            rezGti.setTimeProm(0.0);
        } else {
            rezGti.setTimeProm(getMathRound(rezGti.getTimeProm()));
        }
        if (rezGti.getTimeNarash() == null) {
            rezGti.setTimeNarash(0.0);
        } else {
            rezGti.setTimeNarash(getMathRound(rezGti.getTimeNarash()));
        }
        if (rezGti.getTimeKnbk() == null) {
            rezGti.setTimeKnbk(0.0);
        } else {
            rezGti.setTimeKnbk(getMathRound(rezGti.getTimeKnbk()));
        }
        if (rezGti.getTimeRemont() == null) {
            rezGti.setTimeRemont(0.0);
        } else {
            rezGti.setTimeRemont(getMathRound(rezGti.getTimeRemont()));
        }
        if (rezGti.getTimeGis() == null) {
            rezGti.setTimeGis(0.0);
        } else {
            rezGti.setTimeGis(getMathRound(rezGti.getTimeGis()));
        }
        if (rezGti.getTimeNpv() == null) {
            rezGti.setTimeNpv(0.0);
        } else {
            rezGti.setTimeNpv(getMathRound(rezGti.getTimeNpv()));
        }
        if (rezGti.getTimeProchee() == null) {
            rezGti.setTimeProchee(0.0);
        } else {
            rezGti.setTimeProchee(getMathRound(rezGti.getTimeProchee()));
        }
        return rezGti;
    }
}


