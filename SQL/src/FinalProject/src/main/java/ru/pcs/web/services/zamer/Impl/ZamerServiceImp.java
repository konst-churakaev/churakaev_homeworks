package ru.pcs.web.services.zamer.Impl;

import org.springframework.stereotype.Service;
import ru.pcs.web.model.Zamer;
import ru.pcs.web.services.zamer.ZamerService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ZamerServiceImp implements ZamerService {

    @Override
    public List<Zamer> getFactZamer() {

        List<Zamer> zamersFact = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("zamerFact.txt"))) {
            String line = bufferedReader.readLine();
            while (line != null) {

                String[] parts = line.split("\\;");

                Double depth = Double.parseDouble(parts[0]);
                Double inc = Double.parseDouble(parts[1]);
                Double azimut = Double.parseDouble(parts[2]);
                Double tvd = Double.parseDouble(parts[3]);
                Double north = Double.parseDouble(parts[4]);
                Double west = Double.parseDouble(parts[5]);
                Double closure = Double.parseDouble(parts[6]);

                Zamer newZamer = new Zamer(depth, inc, azimut, tvd, north, west, closure);

                zamersFact.add(newZamer);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(String.format("File with name %s not found with exception %s", "zamerFact.txt", e));
        }
        return zamersFact;
    }

    @Override
    public Zamer lastZamer() {

        List<Zamer> zamers = getFactZamer();
        Zamer lastZamer = zamers.get(zamers.size() - 1);

        return lastZamer;
    }

    @Override
    public List<Zamer> getPlanZamer() {

        List<Zamer> zamersPlan = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("zamerPlan.txt"))) {
            String line = bufferedReader.readLine();
            while (line != null) {

                String[] parts = line.split("\\;");

                Double depth = Double.parseDouble(parts[0]);
                Double inc = Double.parseDouble(parts[1]);
                Double azimut = Double.parseDouble(parts[2]);
                Double tvd = Double.parseDouble(parts[3]);
                Double north = Double.parseDouble(parts[4]);
                Double west = Double.parseDouble(parts[5]);
                Double closure = Double.parseDouble(parts[6]);

                Zamer newZamer = new Zamer(depth, inc, azimut, tvd, north, west, closure);

                zamersPlan.add(newZamer);

                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(String.format("File with name %s not found with exception %s", "zamerPlan.txt", e));
        }
        return zamersPlan;
    }

    @Override
    public Zamer getPlanForFact() {

        List<Zamer> zamersFact = getFactZamer();
        List<Zamer> zamersPlan = getPlanZamer();
        Zamer zamerPlanForFact = zamersFact.get(zamersFact.size() - 1);

        for (int i = 0; i < zamersPlan.size() - 1; i++) {
            if (zamerPlanForFact.getDepth() < zamersPlan.get(i).getDepth()) {
                Double incFact = (zamersPlan.get(i + 1).getInc() - zamersPlan.get(i).getInc()) * (zamerPlanForFact.getDepth() - zamersPlan.get(i).getDepth()) / 10 + zamersPlan.get(i).getInc();
                Double azimutFact = (zamersPlan.get(i + 1).getAzimut() - zamersPlan.get(i).getAzimut()) * (zamerPlanForFact.getDepth() - zamersPlan.get(i).getDepth()) / 10 + zamersPlan.get(i).getAzimut();
                Double tvdFact = (zamersPlan.get(i + 1).getTvd() - zamersPlan.get(i).getTvd()) * (zamerPlanForFact.getDepth() - zamersPlan.get(i).getDepth()) / 10 + zamersPlan.get(i).getTvd();

                zamerPlanForFact.setInc(incFact);
                zamerPlanForFact.setAzimut(azimutFact);
                zamerPlanForFact.setTvd(tvdFact);
            }
        }
        return zamerPlanForFact;
    }
}

