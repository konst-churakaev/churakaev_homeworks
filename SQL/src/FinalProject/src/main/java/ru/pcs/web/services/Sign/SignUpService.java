package ru.pcs.web.services.Sign;

import ru.pcs.web.form.SignUpForm;


public interface SignUpService {
    void signUpUser(SignUpForm form);
}
