package ru.pcs.web.services.Sign.Impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pcs.web.form.SignUpForm;
import ru.pcs.web.model.User;
import ru.pcs.web.repositories.User.UsersRepository;
import ru.pcs.web.services.Sign.SignUpService;

@Component
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final UsersRepository usersRepository;

    @Override
    public void signUpUser(SignUpForm form) {
        User user;
        user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .role(User.Role.ADMIN)
                 .Password(passwordEncoder.encode(form.getPassword()))
                .build();

        usersRepository.save(user);
    }
}

