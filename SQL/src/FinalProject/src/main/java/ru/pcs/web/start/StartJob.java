package ru.pcs.web.start;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class StartJob {
    public static final LocalDate dateOn = LocalDate.of(2021, 12, 19);
    public static final LocalTime timeOn = LocalTime.of(15, 00);
    public static final Double depthOn = 0.0;
}
