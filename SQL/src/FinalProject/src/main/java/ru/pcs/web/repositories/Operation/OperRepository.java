package ru.pcs.web.repositories.Operation;

import ru.pcs.web.model.Oper;

import java.time.LocalDate;
import java.util.List;

public interface OperRepository {

    List<Oper> findAll();

    void save(Oper oper);

    void deleteById(Integer operId);

    Oper findById(Integer operId);

    void updateById(Oper oper, Integer operId);

    Oper lastOper ();

    void updateTimeOper(double timeOper, Integer operId);

    List<Oper> findAllGti(LocalDate yesterday);

}
