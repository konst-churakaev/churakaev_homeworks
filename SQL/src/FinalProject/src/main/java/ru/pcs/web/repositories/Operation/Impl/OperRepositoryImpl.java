package ru.pcs.web.repositories.Operation.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.pcs.web.model.Oper;
import ru.pcs.web.repositories.Operation.OperRepository;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Repository
public class OperRepositoryImpl implements OperRepository {

    private static final String SQL_INSERT = "insert into gti (date_oper, time, timeoper, depth, operation, notes) values (?, ?, ?, ?, ?, ?) ";
    private static final String SQL_SELECT_ALL = "select * from gti ORDER BY date_oper, time";
    private static final String SQL_DELETE_BY_ID = "delete from gti where id=?;";
    private static final String SQL_SELECT_BY_ID = "select * from gti where id =?";
    private static final String SQL_SELECT_BY_LAST_ID = "select * from gti where id = ( select Max(id) from gti)";
    private static final String SQL_SELECT_BY_ID_UPDATE_TIMEOPER = "update gti set timeoper =? where id =?";
    private static final String SQL_SELECT_BY_ID_UPDATE = "update gti set date_oper =?, time =?, timeoper=?, depth =?, operation =?, notes =? where id =?";
    private static final String SQL_SELECT_ALL_DATE ="select * from gti where date_oper = ? order by date_oper, time ";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public OperRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Oper> operRowMapper = (row, rowNumber) -> {

        Integer id = row.getInt("id");
        LocalDate dateOper = row.getDate("date_oper").toLocalDate();
        LocalTime time = row.getTime("time").toLocalTime();
        Double timeOper = row.getDouble("timeOper");
        Double depth = row.getDouble("depth");
        String operation = row.getString("operation");
        String notes = row.getString("notes");

        return new Oper(id, dateOper, time, timeOper, depth, operation, notes);
    };

    @Override
    public List<Oper> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, operRowMapper);
    }

    @Override
    public List<Oper> findAllGti(LocalDate yesterday) {
        return jdbcTemplate.query(SQL_SELECT_ALL_DATE, operRowMapper, yesterday);
    }

    @Override
    public Oper lastOper() {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_LAST_ID, operRowMapper);
    }

    @Override
    public void updateTimeOper(double timeOper, Integer operId) {
        jdbcTemplate.update(SQL_SELECT_BY_ID_UPDATE_TIMEOPER, timeOper, operId);
    }

    @Override
    public void save(Oper oper) {
        jdbcTemplate.update(SQL_INSERT, oper.getDateOper(), oper.getTime(), oper.getTimeOper(), oper.getDepth(), oper.getOperation(), oper.getNotes());
    }

    @Override
    public void deleteById(Integer operId) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, operId);
    }

    @Override
    public Oper findById(Integer operId) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, operRowMapper, operId);
    }

    @Override
    public void updateById(Oper oper, Integer operId) {
        jdbcTemplate.update(SQL_SELECT_BY_ID_UPDATE, oper.getDateOper(), oper.getTime(), oper.getTimeOper(), oper.getDepth(), oper.getOperation(), oper.getNotes(), operId);
    }
}
