package ru.pcs.web.repositories.rezult;

import ru.pcs.web.model.RezGti;

import java.time.LocalDate;

public interface RezGtiRepository {

    RezGti findOperation();

    RezGti getDailyReport(LocalDate yesterday);

}
