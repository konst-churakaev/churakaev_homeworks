package ru.pcs.web.services.rezult;

import ru.pcs.web.model.RezGti;

import java.time.LocalDate;

public interface RezGtiService {

    RezGti getOperation();

    RezGti getDailyReport();

    RezGti setDailyReport(LocalDate operDate);

}
