package ru.pcs.web.repositories.User;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.model.User;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Integer> {

    Optional<User > findByEmail(String email);

}
