package ru.pcs.web.services.operation.Impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.web.form.OperForm;
import ru.pcs.web.model.Oper;
import ru.pcs.web.repositories.Operation.OperRepository;
import ru.pcs.web.services.operation.OperService;
import ru.pcs.web.start.StartJob;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OperServiceImpl implements OperService {

    private final OperRepository operRepository;

    @Override
    public void addNewOper(OperForm form) {

        Oper lastOper = operRepository.lastOper();

        Oper oper = Oper.builder()
                .dateOper(LocalDate.now())
                .time(form.getTime())
                .timeOper(form.getTimeOper())
                .depth(form.getDepth())
                .operation(form.getOperation())
                .notes(form.getNotes())
                .build();

        if (oper.getDepth() < lastOper.getDepth()) {
            oper.setNotes("ОШИБКА ВВОДА ГЛУБИНЫ");
        }
        Duration durationTimeAdd = Duration.between(lastOper.getTime(), oper.getTime());
        double temp = (double) durationTimeAdd.toMinutes() / 60;

        temp = Math.round(temp * 100);
        double timeOper = temp / 100;
        if (timeOper <= 0 && lastOper.getDateOper().equals(oper.getDateOper())) {
            oper.setNotes("ОШИБКА ВВОДА ВРЕМЕНИ");
        }

        if (timeOper < 0) {
            timeOper += 24;
        }
        oper.setTimeOper(timeOper);

        operRepository.save(oper);
    }

    @Override
    public List<Oper> getAllOper() {
        return operRepository.findAll();
    }

    @Override
    public Oper getOper(Integer operId) {
        return operRepository.findById(operId);
    }

    @Override
    public void deleteOper(Integer operId) {

        operRepository.deleteById(operId);
        updateTimeOperGti();
    }

    @Override
    public void updateOper(OperForm form, Integer operId) {

        Oper oper = operRepository.findById(operId);
        oper.setDateOper(LocalDate.parse(form.getDateOper()));
        oper.setTime(form.getTime());
        oper.setTimeOper(form.getTimeOper());
        oper.setDepth(form.getDepth());
        oper.setOperation(form.getOperation());
        oper.setNotes(form.getNotes());

        operRepository.updateById(oper, operId);
        updateTimeOperGti();
    }

    private void updateTimeOperGti() {

        List<Oper> opers = operRepository.findAll();
        LocalTime timeOn = StartJob.timeOn;

        for (int i = 1; i < opers.size(); i++) {

            Duration durationTime = Duration.between(timeOn, opers.get(i).getTime());
            double temp = (double) durationTime.toMinutes() / 60;

            temp = Math.round(temp * 100);
            double timeOper = temp / 100;

            if (timeOper < 0) {
                timeOper += 24;
            }
            operRepository.updateTimeOper(timeOper, opers.get(i).getId());

            timeOn = opers.get(i).getTime();
        }
    }
}
