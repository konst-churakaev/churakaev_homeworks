package ru.pcs.web.repositories.rezult.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.pcs.web.model.RezGti;
import ru.pcs.web.repositories.rezult.RezGtiRepository;

import javax.sql.DataSource;
import java.time.LocalDate;


@Repository
public class RezGtiRepositoryImp implements RezGtiRepository {

    private static final String SQL_GET_TIME_ALL = "select sum(timeOper) from gti";
    private static final String SQL_GET_TIME_OPERATION = "select sum(timeOper) from gti where operation = ?";
    private static final String SQL_GET_TIME_CYCLE = "select sum(timeOper) from gti where operation IN ('Замер', 'Бурение', 'Проработка', 'Промывка')";
    private static final String SQL_GET_AVG_SPEED = "select  (max(depth) - min(depth))/sum (timeoper) from gti where operation = 'Бурение'";
    private static final String SQL_GET_TIME_ALL_DATE = "select sum(timeOper) from gti where date_oper = ?";
    private static final String SQL_GET_TIME_OPERATION_DATE = "select sum(timeOper) from gti where operation = ? and date_oper = ?";
    private static final String SQL_GET_TIME_CYCLE_DATE = "select sum(timeOper) from gti where operation IN ('Замер', 'Бурение', 'Проработка', 'Промывка') and date_oper = ?";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public RezGtiRepositoryImp(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public RezGti findOperation() {

        RezGti rezGti = new RezGti();

        rezGti.setTimeAll(jdbcTemplate.queryForObject(SQL_GET_TIME_ALL, Double.class ));
        rezGti.setTimeCycle(jdbcTemplate.queryForObject(SQL_GET_TIME_CYCLE, Double.class ));
        rezGti.setTimeByr(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "Бурение" ));
        rezGti.setTimeSpo(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "СПО" ));
        rezGti.setTimeZamer(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "Замер" ));
        rezGti.setTimeProrabot(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "Проработка" ));
        rezGti.setTimeProm(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "Промывка" ));
        rezGti.setTimeNarash(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "Наращивание" ));
        rezGti.setTimeKnbk(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "КНБК" ));
        rezGti.setTimeRemont(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "Ремонт" ));
        rezGti.setTimeGis(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "ГИС" ));
        rezGti.setTimeNpv(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "НПВ" ));
        rezGti.setTimeProchee(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION, Double.class, "Прочее" ));

        return rezGti;
       }

    @Override
    public RezGti getDailyReport(LocalDate yesterday) {

        RezGti rezGtiDailyReport = new RezGti();

        rezGtiDailyReport.setTimeAll(jdbcTemplate.queryForObject(SQL_GET_TIME_ALL_DATE, Double.class, yesterday ));
        rezGtiDailyReport.setTimeCycle(jdbcTemplate.queryForObject(SQL_GET_TIME_CYCLE_DATE, Double.class, yesterday ));
        rezGtiDailyReport.setTimeByr(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "Бурение", yesterday ));
        rezGtiDailyReport.setTimeSpo(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "СПО", yesterday ));
        rezGtiDailyReport.setTimeZamer(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "Замер", yesterday ));
        rezGtiDailyReport.setTimeProrabot(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "Проработка", yesterday ));
        rezGtiDailyReport.setTimeProm(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "Промывка", yesterday ));
        rezGtiDailyReport.setTimeNarash(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "Наращивание", yesterday ));
        rezGtiDailyReport.setTimeKnbk(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "КНБК", yesterday ));
        rezGtiDailyReport.setTimeRemont(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "Ремонт", yesterday ));
        rezGtiDailyReport.setTimeGis(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "ГИС", yesterday ));
        rezGtiDailyReport.setTimeNpv(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "НПВ", yesterday ));
        rezGtiDailyReport.setTimeProchee(jdbcTemplate.queryForObject(SQL_GET_TIME_OPERATION_DATE, Double.class, "Прочее", yesterday ));

        return rezGtiDailyReport;
    }
}