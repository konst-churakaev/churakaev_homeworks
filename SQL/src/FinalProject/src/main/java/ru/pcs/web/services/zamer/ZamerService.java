package ru.pcs.web.services.zamer;

import ru.pcs.web.model.Zamer;

import java.util.List;

public interface ZamerService {

    List<Zamer> getFactZamer();

    Zamer lastZamer();

    List<Zamer> getPlanZamer();

    Zamer getPlanForFact();
}