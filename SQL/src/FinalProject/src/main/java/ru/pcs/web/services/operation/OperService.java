package ru.pcs.web.services.operation;

import ru.pcs.web.form.OperForm;
import ru.pcs.web.model.Oper;

import java.util.List;

public interface OperService {

    void addNewOper(OperForm form);

    List<Oper> getAllOper();

    void deleteOper(Integer operId);

    Oper getOper(Integer operId);

    void updateOper(OperForm operForm, Integer operId);
}
