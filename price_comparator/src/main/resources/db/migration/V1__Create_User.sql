CREATE TABLE IF NOT EXISTS users(
id int NOT NULL PRIMARY KEY,
name varchar(20),
email varchar(50),
date_of_birth timestamp)