CREATE TABLE IF NOT EXISTS product_category(
id bigint NOT NULL PRIMARY KEY,
name varchar(50) NOT NULL
);
