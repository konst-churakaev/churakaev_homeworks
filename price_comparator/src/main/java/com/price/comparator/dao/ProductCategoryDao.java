package com.price.comparator.dao;

import com.price.comparator.model.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductCategoryDao extends JpaRepository<ProductCategory, Long> {
}
