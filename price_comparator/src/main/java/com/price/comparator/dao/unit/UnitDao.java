package com.price.comparator.dao.unit;


import com.price.comparator.model.unit.Unit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnitDao extends JpaRepository<Unit, Long> {

}
