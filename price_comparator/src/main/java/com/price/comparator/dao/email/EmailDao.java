package com.price.comparator.dao.email;

import com.price.comparator.model.email.Email;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailDao extends JpaRepository<Email, Long> {
}
