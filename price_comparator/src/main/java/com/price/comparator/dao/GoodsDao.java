package com.price.comparator.dao;

import com.price.comparator.model.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsDao extends JpaRepository <Goods, Long> {
}
