package com.price.comparator.dao.medicine;

import com.price.comparator.model.medicine.Medicine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicineDao extends JpaRepository<Medicine,Long> {
}
