package com.price.comparator.controller;

import com.price.comparator.model.email.Email;
import com.price.comparator.service.email.EmailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(EmailController.PATH)
public class EmailController {

    public static final String PATH = "/api/email/";

    private EmailServiceImpl emailService;

    @Autowired
    public EmailController(EmailServiceImpl emailServiceImpl) {
        this.emailService = emailServiceImpl;
    }

    @PostMapping()
    public Email add(@RequestBody Email email) {
        return emailService.add(email);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Long id) {
        emailService.delete(id);
    }

}
