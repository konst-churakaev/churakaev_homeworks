package com.price.comparator.controller;

import com.price.comparator.model.Goods;
import com.price.comparator.service.GoodsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(GoodsController.PATH)
public class GoodsController {

    public static final String PATH = "/api/goods/";

    private final GoodsService goodsService;

    public GoodsController(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @GetMapping
    public List<Goods> getAllGoods() {
        return goodsService.getAll();
    }

    @PostMapping()
    public Goods addGoods(@RequestBody Goods goods) {
        return goodsService.add(goods);
    }

    @PutMapping()
    public Goods saveGoods(@RequestBody Goods goods) {
        return goodsService.update(goods);
    }

    @DeleteMapping("{id}")
    public List<Goods> deleteGoods(@PathVariable("id") Long id) {
        return goodsService.deleteById(id);
    }

    @GetMapping("{id}")
    public Goods getGoodsById(@PathVariable("id") Long id) {
        return goodsService.getById(id);
    }
}
