package com.price.comparator.controller;


import com.price.comparator.model.ProductCategory;
import com.price.comparator.service.ProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(ProductCategoryController.PATH)
public class ProductCategoryController {

    public static final String PATH = "/api/product-category/";

    private final ProductCategoryService productCategoryService;

    @Autowired
    public ProductCategoryController(ProductCategoryService productCategoryService) {
        this.productCategoryService = productCategoryService;
    }

    @GetMapping
    public List<ProductCategory> showAllProductsCategory() {
        return productCategoryService.getAll();
    }

    @GetMapping("{id}")
    public ProductCategory getProductCategoryById(@PathVariable("id") Long id) {
        return productCategoryService.getById(id);
    }

}
