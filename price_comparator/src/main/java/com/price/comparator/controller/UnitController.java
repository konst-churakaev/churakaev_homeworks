package com.price.comparator.controller;

import com.price.comparator.model.unit.Unit;
import com.price.comparator.service.unit.UnitService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(UnitController.PATH)
public class UnitController {

    public static final String PATH = "/api/unit/";

    private final UnitService unitService;

    public UnitController(UnitService unitService) {
        this.unitService = unitService;
    }

    @GetMapping
    public List<Unit> getAllUnits(){
        return unitService.getAll();
    }

    @PostMapping()
    public Unit addUnit(@RequestBody Unit unit){
        return unitService.add(unit);
    }

    @PutMapping()
    public Unit modifyUnit (@RequestBody Unit unit){
        return unitService.update(unit);
    }

    @DeleteMapping("{id}")
    public List<Unit> deleteUnit(@PathVariable("id") Long id){
        return unitService.deleteById(id);
    }

    @GetMapping("{id}/")
    public Unit getUnitById(@PathVariable("id") Long id){
        return unitService.getById(id);
    }
}

