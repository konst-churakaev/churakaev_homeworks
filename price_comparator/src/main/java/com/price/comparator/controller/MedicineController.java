package com.price.comparator.controller;

import com.price.comparator.model.medicine.Medicine;
import com.price.comparator.service.medicine.MedicineService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(MedicineController.PATH)
public class MedicineController {

    public static final String PATH  = "/api/medicine/";

    private final MedicineService medicineService;

    public MedicineController(MedicineService medicineService) {
        this.medicineService = medicineService;
    }

    @GetMapping
    public List<Medicine> getAllMedicine(){
        return medicineService.getAll();
    }

    @GetMapping("{id}")
    public Medicine getMedicineById(@PathVariable("id") Long id){
        return medicineService.getById(id);
    }

    @PostMapping
    public Medicine addMedicine(@RequestBody Medicine medicine){
        return medicineService.add(medicine);
    }

    @DeleteMapping("{id}")
    public List<Medicine> deleteMedicine(@PathVariable("id") Long id){
        return medicineService.deleteById(id);
    }

    @PutMapping
    public Medicine updateMedicine(@RequestBody Medicine medicine){
        return medicineService.update(medicine);
    }
}
