package com.price.comparator.model.email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "mails")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Email implements Serializable {

    private static final long serialVersionUID = -1573800493151697558L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "email")
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Email e = (Email) o;
        return Objects.equals(id, e.id) && Objects.equals(email, e.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
