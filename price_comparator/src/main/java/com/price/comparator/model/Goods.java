package com.price.comparator.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.price.comparator.model.unit.Unit;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "goods")
@JsonIgnoreProperties ({"hibernateLazyInitializer", "handler"})
public class Goods implements Serializable {

    private static final long serialVersionUID = 5857626791135950390L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn (name = "product_category_id")
    private ProductCategory productCategory;

    @Column(name = "name_goods", nullable = false)
    private String nameGoods;

    @ManyToOne
    @JoinColumn (name = "units_id")
    private Unit units;

    @Column (name = "numbers")
    private Integer numbers;

    @Column (name = "composition")
    private String composition;

    @Column (name = "price", scale = 2)
    private BigDecimal price;

    @Column (name = "manufacturer")
    private String manufacturer;

    @Column (name = "salesman")
    private String salesman;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Goods goods = (Goods) o;
        return Objects.equals(id, goods.id) && Objects.equals(productCategory, goods.productCategory) && Objects.equals(nameGoods, goods.nameGoods) &&
                Objects.equals(units, goods.units) && Objects.equals(numbers, goods.numbers) && Objects.equals(composition, goods.composition) &&
                Objects.equals(price, goods.price) && Objects.equals(manufacturer, goods.manufacturer) && Objects.equals(salesman, goods.salesman);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public String getNameGoods() {
        return nameGoods;
    }

    public void setNameGoods(String nameGoods) {
        this.nameGoods = nameGoods;
    }

    public Unit getUnits() {
        return units;
    }

    public void setUnits(Unit units) {
        this.units = units;
    }

    public Integer getNumbers() {
        return numbers;
    }

    public void setNumbers(Integer numbers) {
        this.numbers = numbers;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }
}
