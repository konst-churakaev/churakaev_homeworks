package com.price.comparator.service;

import com.price.comparator.model.Goods;

import java.util.List;

public interface GoodsService {

    List<Goods> getAll();

    Goods add(Goods goods);

    Goods update(Goods goods);

    List<Goods> deleteById(Long id);

    Goods getById(Long id);
}
