package com.price.comparator.service;

import com.price.comparator.model.ProductCategory;

import java.util.List;

public interface ProductCategoryService {
    List<ProductCategory> getAll();
    ProductCategory getById(Long id);
    ProductCategory add(ProductCategory productCategory);
    ProductCategory update(ProductCategory productCategory);
    void delete(Long id);

}
