package com.price.comparator.service.email;

import com.price.comparator.model.email.Email;
import com.price.comparator.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Random;

@Service
public class EmailSenderService {

    @Autowired
    private JavaMailSender emailSender;
    @Autowired
    private EmailServiceImpl emailService;
    @Autowired
    private GoodsService goodsService;

    @Scheduled(fixedDelay = 2*60*1000) //запускается каждые 2 минуты
    public void sendSimpleMessage() {

        int number = Math.min(goodsService.getAll().size(), 10);
        Random random = new Random();
        HashSet<Long> set = new HashSet<>(); // для уникальных 10-ти рандомных чисел
        while (set.size()!=number) {
            long i = random.nextLong(number);
            i = i + 1L; // чтобы id не был равен нулю
            set.add(i);
        }

        StringBuilder textForMessage = new StringBuilder("Приветствую, друг!\n\nЭти товары по скидке могут заинтересовать тебя:\n");
        for (Long id:set) {
            textForMessage.append(goodsService.getById(id).getNameGoods());
            textForMessage.append(" - ");
            textForMessage.append(goodsService.getById(id).getPrice().toString());
            textForMessage.append(" руб.\n");
        }

        SimpleMailMessage message = new SimpleMailMessage();
        for (Email email:emailService.getAll()) {
            message.setFrom("createservicevaleria@gmail.com");
            message.setSubject("Ежедневная рассылка");
            message.setText(textForMessage.toString());
            message.setTo(email.getEmail());
            emailSender.send(message);
        }
    }
}
