package com.price.comparator.service.medicine.impl;

import com.price.comparator.dao.medicine.MedicineDao;
import com.price.comparator.model.medicine.Medicine;
import com.price.comparator.service.medicine.MedicineService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MedicineServiceImpl implements MedicineService {

    private final MedicineDao medicineDao;

    public MedicineServiceImpl(MedicineDao medicineDao) {
        this.medicineDao = medicineDao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Medicine> getAll() {
        return medicineDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Medicine getById(Long id) {
        return medicineDao.getById(id);
    }

    @Override
    @Transactional
    public Medicine add(Medicine medicine) {
        return medicineDao.save(medicine);
    }

    @Override
    @Transactional
    public List<Medicine> deleteById(Long id) {
        medicineDao.deleteById(id);
        return medicineDao.findAll();
    }

    @Override
    @Transactional
    public Medicine update(Medicine medicine) {
        return medicineDao.save(medicine);
    }
}
