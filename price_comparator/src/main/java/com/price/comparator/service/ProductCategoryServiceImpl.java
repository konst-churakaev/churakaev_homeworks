package com.price.comparator.service;

import com.price.comparator.model.ProductCategory;
import com.price.comparator.dao.ProductCategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

    private final ProductCategoryDao productCategoryDao;

    @Autowired
    public ProductCategoryServiceImpl(ProductCategoryDao productCategoryDao) {
        this.productCategoryDao = productCategoryDao;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ProductCategory> getAll() {
        return productCategoryDao.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public ProductCategory getById(Long id) {
        return productCategoryDao.getById(id);
    }

    @Transactional
    @Override
    public ProductCategory add(ProductCategory productCategory) {
        return productCategoryDao.save(productCategory);
    }

    @Transactional
    @Override
    public ProductCategory update(ProductCategory productCategory) {
        return productCategoryDao.save(productCategory);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        productCategoryDao.deleteById(id);
    }
}
