package com.price.comparator.service.unit;


import com.price.comparator.model.unit.Unit;

import java.util.List;

public interface UnitService {

    List<Unit> getAll();

    Unit add(Unit unit);

    Unit getById(Long id);

    List<Unit> deleteById(Long id);

    Unit update(Unit unit);
}
