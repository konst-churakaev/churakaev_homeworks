package com.price.comparator.service.email;

import com.price.comparator.dao.email.EmailDao;
import com.price.comparator.model.email.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EmailServiceImpl implements EmailService {

    private final EmailDao emailDao;

    @Autowired
    public EmailServiceImpl(EmailDao emailDao) {
        this.emailDao = emailDao;
    }

    @Override
    @Transactional
    public Email add(Email email) {
        return emailDao.save(email);
    }

    @Override
    @Transactional (readOnly = true)
    public Email getById(Long id) {
        return emailDao.getById(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        emailDao.deleteById(id);
    }


    @Override
    @Transactional (readOnly = true)
    public List<Email> getAll() {
        return emailDao.findAll();
    }

    @Override
    @Transactional
    public Email update(Email email) {
        return emailDao.save(email);
    }
}
