package com.price.comparator.service.unit.impl;

import com.price.comparator.dao.unit.UnitDao;
import com.price.comparator.model.unit.Unit;
import com.price.comparator.service.unit.UnitService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class UnitServiceImpl implements UnitService {

    private final UnitDao unitDao;

    public UnitServiceImpl(UnitDao unitDao) {
        this.unitDao = unitDao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Unit> getAll() {
        return unitDao.findAll();
    }

    @Override
    @Transactional
    public Unit add(Unit unit) {
        return unitDao.save(unit);
    }

    @Override
    @Transactional(readOnly = true)
    public Unit getById(Long id) {
        return unitDao.getById(id);
    }

    @Override
    @Transactional
    public List<Unit> deleteById(Long id) {
        unitDao.deleteById(id);
        return unitDao.findAll();
    }

    @Override
    @Transactional
    public Unit update(Unit unit) {
        return unitDao.save(unit);
    }
}
