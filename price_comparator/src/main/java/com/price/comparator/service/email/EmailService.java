package com.price.comparator.service.email;

import com.price.comparator.model.email.Email;

import java.util.List;

public interface EmailService {

    Email add(Email email);

    Email getById(Long id);

    void delete(Long id);

    List<Email> getAll();

    Email update(Email email);
}
