package com.price.comparator.service.medicine;

import com.price.comparator.model.medicine.Medicine;

import java.util.List;

public interface MedicineService {

    List<Medicine> getAll();

    Medicine getById(Long id);

    Medicine add(Medicine medicine);

    List<Medicine> deleteById(Long id);

    Medicine update(Medicine medicine);
}