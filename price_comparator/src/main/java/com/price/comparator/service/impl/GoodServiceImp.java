package com.price.comparator.service.impl;

import com.price.comparator.dao.GoodsDao;
import com.price.comparator.model.Goods;
import com.price.comparator.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GoodServiceImp implements GoodsService {

    @Autowired
    private final GoodsDao goodsDao;

    public GoodServiceImp(GoodsDao goodsDao) {
        this.goodsDao = goodsDao;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Goods> getAll() {
        return goodsDao.findAll();
    }

    @Override
    @Transactional
    public Goods add(Goods goods) {
        return goodsDao.save(goods);
    }

    @Override
    @Transactional
    public Goods update(Goods goods) {
        return goodsDao.save(goods);
    }

    @Override
    @Transactional
    public List<Goods> deleteById(Long id) {
        goodsDao.deleteById(id);
        return goodsDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Goods getById(Long id) {
        return goodsDao.getById(id);
    }
}
