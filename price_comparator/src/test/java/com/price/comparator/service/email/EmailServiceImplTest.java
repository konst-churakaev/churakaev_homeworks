package com.price.comparator.service.email;

import com.price.comparator.model.email.Email;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@SpringBootTest
@Transactional
@Sql(value = {"/create-email-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class EmailServiceImplTest {

    @Autowired
    private EmailService emailService;

    @Test
    void add() {

        Email email = new Email();
        email.setId(3L);
        email.setEmail("user3@gmail.com");

        Email testEmail = emailService.add(email);
        List<Email> all = emailService.getAll();

        Assertions.assertEquals(4, all.size());
        Assertions.assertEquals(3L, testEmail.getId());
        Assertions.assertEquals("user3@gmail.com", testEmail.getEmail());
    }

    @Test
    void getById() {

        Email email = emailService.getById(2L);

        Assertions.assertEquals(2L, email.getId());
        Assertions.assertEquals("user2@gmail.com", email.getEmail());
    }

    @Test
    void delete() {

        emailService.delete(2L);
        List<Email> all = emailService.getAll();

        Set<Long> ids = all.stream().map(e -> e.getId()).collect(toSet());
        Assertions.assertFalse(ids.contains(2L));



    }

    @Test
    void getAll() {

        List<Email> all = emailService.getAll();

        Assertions.assertEquals(1L, all.get(0).getId());
        Assertions.assertEquals(2L, all.get(1).getId());
        Assertions.assertEquals(10L, all.get(2).getId());
        Assertions.assertEquals("user1@gmail.com", all.get(0).getEmail());
        Assertions.assertEquals("user2@gmail.com", all.get(1).getEmail());
        Assertions.assertEquals("user10@gmail.com", all.get(2).getEmail());

    }

    @Test
    void update() {

        Email email = emailService.getById(1L);
        email.setEmail("user4@gmail.com");

        Email testEmail = emailService.update(email);

        Email updatedEmail = emailService.getById(1L);

        Assertions.assertEquals("user4@gmail.com", updatedEmail.getEmail());
    }
}