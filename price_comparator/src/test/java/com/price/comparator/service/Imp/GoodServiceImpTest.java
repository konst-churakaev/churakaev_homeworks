package com.price.comparator.service.Imp;

import com.price.comparator.model.Goods;
import com.price.comparator.service.GoodsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@SpringBootTest
//@Sql(value = {"/create-goods-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Transactional
class GoodServiceImpTest {

    @Autowired
    private GoodsService goodsService;

    @Test
    void getAll() {

//        List<Goods> all = goodsService.getAll();

        Goods goodsTest = new Goods();
        goodsTest.setId(1L);

        Assertions.assertEquals(1L, goodsTest.getId());
//        Assertions.assertEquals(1L, all.get(0).getProductCategory());
//        Assertions.assertEquals("beer", all.get(0).getNameGoods());
//        Assertions.assertEquals(1L, all.get(0).getUnits());
//        Assertions.assertEquals(10, all.get(0).getNumbers());
//        Assertions.assertEquals("beer", all.get(0).getComposition());
//        Assertions.assertEquals(10.0, all.get(0).getPrice());
//        Assertions.assertEquals("Russia", all.get(0).getManufacturer());
//        Assertions.assertEquals("Lenta", all.get(0).getSalesman());
//        Assertions.assertEquals(9, all.size());
    }

    @Test
    void add() {

          Goods goodsTest = new Goods();
        goodsTest.setNameGoods("milk");

        Assertions.assertEquals("milk", goodsTest.getNameGoods());

    }

    @Test
    void update() {
    }

    @Test
    void deleteById() {
    }

    @Test
    void getById() {
    }
}