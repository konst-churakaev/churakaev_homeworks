package com.price.comparator.service.unit.impl;


import com.price.comparator.model.unit.Unit;
import com.price.comparator.service.unit.UnitService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
@Sql(value = {"/create-unit-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-unit-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Transactional
public class UnitServiceImplTest {


    @Autowired
    private UnitService service;

    @Test
    void getAll(){

        List<Unit> all = service.getAll();

        Assertions.assertEquals(1L, (long) all.get(0).getId());
        Assertions.assertEquals("шт.", all.get(0).getName());
        Assertions.assertEquals(2, all.size());
    }

    @Test
    void add(){
        Unit unit = new Unit();
        unit.setName("?.");

        Unit testUnit = service.add(unit);
        List<Unit> all = service.getAll();

        Assertions.assertEquals("?.", testUnit.getName());
        Assertions.assertEquals(3, all.size());
    }

    @Test
    void getById(){

        Unit unit = service.getById(1L);

        Assertions.assertEquals(1L, unit.getId());
        Assertions.assertEquals("шт.", unit.getName());
    }

    @Test
    void deleteById(){

        List<Unit> all = service.deleteById(1L);

        Assertions.assertEquals(1, all.size());
        Assertions.assertEquals("кг.", all.get(0).getName());
    }

    @Test
    void update(){

        Unit unit = new Unit();
        unit.setName("q");
        unit.setId(1L);

        Unit unitTest = service.update(unit);

        Assertions.assertEquals("q", unitTest.getName());
    }
}

