package com.price.comparator.service.medicine.impl;

import com.price.comparator.model.medicine.Medicine;
import com.price.comparator.service.medicine.MedicineService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@SpringBootTest
@Sql(value = {"/create-medicine-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Transactional
public class MedicineServiceImplTest {

    @Autowired
    MedicineService medicineService;

    @Test
    void getAll(){

        List<Medicine> all = medicineService.getAll();

        Assertions.assertEquals(all.size(),2);
        Assertions.assertEquals(all.get(0).getId(),1);
        Assertions.assertEquals(all.get(0).getName(),"имя1");
        Assertions.assertEquals(all.get(0).getForm(),"форма1");
        Assertions.assertEquals(all.get(0).getPrice(),BigDecimal.valueOf(100.0));
        Assertions.assertEquals(all.get(1).getId(),2);
        Assertions.assertEquals(all.get(1).getName(),"имя2");
        Assertions.assertEquals(all.get(1).getForm(),"форма2");
        Assertions.assertEquals(all.get(1).getPrice(),BigDecimal.valueOf(200.0));
    }

    @Test
    void getById(){

        Medicine medicine = medicineService.getById(2L);

        Assertions.assertEquals(medicine.getId(),2);
        Assertions.assertEquals(medicine.getName(),"имя2");
        Assertions.assertEquals(medicine.getForm(),"форма2");
        Assertions.assertEquals(medicine.getPrice(),BigDecimal.valueOf(200.0));
    }

    @Test
    void add(){

        Medicine medicine = new Medicine();
        medicine.setName("q");
        medicine.setForm("w");
        medicine.setPrice(BigDecimal.valueOf(1.0));

        Medicine medicineAdded = medicineService.add(medicine);

        Assertions.assertEquals(medicineAdded.getId(),3);
        Assertions.assertEquals(medicineAdded.getName(),"q");
        Assertions.assertEquals(medicineAdded.getForm(),"w");
        Assertions.assertEquals(medicineAdded.getPrice(),BigDecimal.valueOf(1.0));
    }

    @Test
    void deleteById(){

        List<Medicine> all = medicineService.deleteById(1L);

        Assertions.assertEquals(all.size(),1);
        Assertions.assertEquals(all.get(0).getId(),2);
        Assertions.assertEquals(all.get(0).getName(),"имя2");
        Assertions.assertEquals(all.get(0).getForm(),"форма2");
        Assertions.assertEquals(all.get(0).getPrice(),BigDecimal.valueOf(200.0));
    }

    @Test
    void update(){
        Medicine medicine = new Medicine();
        medicine.setId(1L);
        medicine.setName("q");
        medicine.setForm("w");
        medicine.setPrice(BigDecimal.valueOf(1.0));

        Medicine medicineUpdated = medicineService.add(medicine);

        Assertions.assertEquals(medicineUpdated.getId(),1);
        Assertions.assertEquals(medicineUpdated.getName(),"q");
        Assertions.assertEquals(medicineUpdated.getForm(),"w");
        Assertions.assertEquals(medicineUpdated.getPrice(),BigDecimal.valueOf(1.0));
    }
}
