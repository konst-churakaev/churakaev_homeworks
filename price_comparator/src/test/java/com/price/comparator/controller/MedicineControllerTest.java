package com.price.comparator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.price.comparator.model.medicine.Medicine;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class})
@Sql(value = {"/create-medicine-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class MedicineControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    void getAllMedicine() throws Exception {

        mockMvc.perform(get(MedicineController.PATH)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("имя1")))
                .andExpect(jsonPath("$[0].form", is("форма1")))
                .andExpect(jsonPath("$[0].price", is(100.0)))
                .andExpect(jsonPath("$[1].id", is(2)));
    }

    @Test
    void getMedicineById() throws Exception {

        mockMvc.perform(get(MedicineController.PATH + "{id}/", 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("имя1")));
    }

    @Test
    void addMedicine() throws Exception{

        Medicine medicine = new Medicine();
        medicine.setName("q");
        medicine.setForm("w");
        medicine.setPrice(BigDecimal.valueOf(300));

        String requestJson=mapper.writeValueAsString(medicine);

        mockMvc.perform(post(MedicineController.PATH)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(3)))
                .andExpect(jsonPath("$.name", is("q")))
                .andExpect(jsonPath("$.form", is("w")))
                .andExpect(jsonPath("$.price", is(300)));
    }

    @Test
    void deleteMedicine() throws Exception{

        mockMvc.perform(delete(MedicineController.PATH + "{id}",1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is("имя2")))
                .andExpect(jsonPath("$[0].id", is(2)));
    }

    @Test
    void updateMedicine() throws Exception{
        Medicine medicine = new Medicine();
        medicine.setId(1L);
        medicine.setName("q");
        medicine.setForm("w");
        medicine.setPrice(BigDecimal.valueOf(300.0));

        String requestJson=mapper.writeValueAsString(medicine);

        mockMvc.perform(post(MedicineController.PATH)
                        .content(requestJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("q")))
                .andExpect(jsonPath("$.form", is("w")))
                .andExpect(jsonPath("$.price", is(300.0)));
    }
}
